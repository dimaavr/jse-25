package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @Nullable final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

}