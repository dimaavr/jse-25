package ru.tsc.avramenko.tm.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasStartDate {

    @Nullable
    Date getStartDate();

    void setStartDate(@Nullable Date startDate);

}
